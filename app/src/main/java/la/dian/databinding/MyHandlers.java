package la.dian.databinding;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

/**
 * User: Aaron
 * Date: 2016/2/2
 * Time: 16:15
 */
public class MyHandlers {
    private static final String TAG = "MyHandlers";

    private Context context;
    private User user;

    public MyHandlers(Context context, User user) {
        this.context = context;
        this.user = user;
    }

    public void onClickFriend(View view) {
        Log.e(TAG, "onClickFriend");
        user.setLastName("onClickFriend");
        Toast.makeText(context, view.getId() + "", Toast.LENGTH_SHORT).show();
    }

    public void onClickEnemy(View view) {
        Log.e(TAG, "onClickEnemy");
        user.setLastName("onClickEnemy");
        Toast.makeText(context, view.getId() + "", Toast.LENGTH_SHORT).show();

    }
}