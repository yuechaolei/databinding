package la.dian.databinding;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.hardware.display.DisplayManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import dalvik.system.DexClassLoader;

public class ProxyActivity extends Activity {

    private static final String TAG = "ProxyActivity";

    public static final String FROM = "extra.from";
    public static final int FROM_EXTERNAL = 0;
    public static final int FROM_INTERNAL = 1;

    public static final String EXTRA_DEX_PATH = "extra.dex.path";
    public static final String EXTRA_CLASS = "extra.class";

    //private String mClass;
    private String mDexPath = "/mnt/sdcard/adss.apk";
    private AssetManager mAssetManager;
    private Resources mResources;
    private Resources.Theme mTheme;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);
        Test test = new Test();
        Log.d("JNI", test.test());
        loadResources();
        //loadResources();
        // mClass = getIntent().getStringExtra(EXTRA_CLASS);

        //Log.d(TAG, "mClass=" + mClass + " mDexPath=" + mDexPath);
//        if (mClass == null) {
//            //launchTargetActivity();
//        } else {
//            launchTargetActivity(mClass);
//        }
        launchTargetActivity("la.dian.adss.MPresentation");
    }

//    @SuppressLint("NewApi")
//    protected void launchTargetActivity() {
//
//        PackageInfo packageInfo = getPackageManager().getPackageArchiveInfo(
//                mDexPath, 1);
//
//        if ((packageInfo.activities != null)
//                && (packageInfo.activities.length > 0)) {
//            String activityName = packageInfo.activities[0].name;
//            mClass = activityName;
//            launchTargetActivity(mClass);
//        }
//    }

    @SuppressLint("NewApi")
    protected void launchTargetActivity(final String className) {
        // la.dian.adss.MPresentation
        Log.e(TAG, "start launchTargetActivity, className=" + className);
        File dexOutputDir = this.getDir("dex", 0);
        final String dexOutputPath = dexOutputDir.getAbsolutePath();
        ClassLoader localClassLoader = ClassLoader.getSystemClassLoader();
        DexClassLoader dexClassLoader = new DexClassLoader(mDexPath,
                dexOutputPath, null, localClassLoader);
        try {
            Class<?> localClass = dexClassLoader.loadClass(className);
            Constructor<?> localConstructor = localClass
                    .getConstructor(Activity.class, Display.class);

            DisplayManager displayManager = (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);
            Display[] presentationDisplays = displayManager.getDisplays();
//            if (presentationDisplays.length > 1) {
//                mPresentation = new MPresentation(this, presentationDisplays[1]);
//                mPresentation.show();
//            }
            //activity  display
            Object instance = localConstructor.newInstance(new Object[]{this, presentationDisplays[1]});
            Log.d(TAG, "instance = " + instance);

//            Method setProxy = localClass.getMethod("setProxy",
//                    new Class[]{Activity.class});
//            setProxy.setAccessible(true);
//            setProxy.invoke(instance, new Object[]{this});

            Method onCreate = localClass.getDeclaredMethod("onCreate",
                    new Class[]{Bundle.class});
            onCreate.setAccessible(true);
            Bundle bundle = new Bundle();
            bundle.putInt(FROM, FROM_EXTERNAL);
            onCreate.invoke(instance, new Object[]{bundle});
            Method show = localClass.getDeclaredMethod("show",
                    new Class[]{});
            show.invoke(instance, new Object[]{});
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void loadResources() {
        try {
            AssetManager assetManager = AssetManager.class.newInstance();
            Method addAssetPath = assetManager.getClass().getMethod("addAssetPath", String.class);
            addAssetPath.invoke(assetManager, mDexPath);
            mAssetManager = assetManager;
        } catch (Exception e) {
            e.printStackTrace();
        }
        Resources superRes = super.getResources();
        mResources = new Resources(mAssetManager, superRes.getDisplayMetrics(),
                superRes.getConfiguration());
        mTheme = mResources.newTheme();
        mTheme.setTo(super.getTheme());
    }

    @Override
    public AssetManager getAssets() {
        return mAssetManager == null ? super.getAssets() : mAssetManager;
    }

    @Override
    public Resources getResources() {
        return mResources == null ? super.getResources() : mResources;
    }

}