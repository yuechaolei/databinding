package la.dian.databinding;

/**
 * User: Aaron
 * Date: 2016/2/20
 * Time: 10:27
 */
public class Test {
    private static final String TAG = "Test";

    static {
        System.loadLibrary("test");
    }
    public native String test();
}
