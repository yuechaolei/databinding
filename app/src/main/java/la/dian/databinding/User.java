package la.dian.databinding;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

/**
 * User: Aaron
 * Date: 2016/2/2
 * Time: 15:39
 */
public class User extends BaseObservable {
    private static final String TAG = "User";

    private String firstName;
    private String lastName;

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Bindable
    public String getFirstName() {
        return this.firstName;
    }

    @Bindable
    public String getLastName() {
        return this.lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        notifyPropertyChanged(la.dian.databinding.BR.firstName);
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        notifyPropertyChanged(la.dian.databinding.BR.lastName);
    }

    public boolean isFriend() {
        return firstName.equals("Testttt");
    }
}
